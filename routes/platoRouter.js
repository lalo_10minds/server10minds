

const express = require ('express');
const bodyParser = require('body-parser');
const mongoose = require ('mongoose');

var authenticate = require('../authenticate');

const Platos = require ('../models/platos');

const platoRouter = express.Router();

platoRouter.use(bodyParser.json());

platoRouter.route('/')
.get ((req, res, next)=>{
  Platos.find({})
  .then((platos)=>{
    res.statusCode = 200;
    res.setHeader('Content-Type','application/json');
    res.json(platos);
  }, (err) => next(err))
  .catch((err) => next(err));
})
.post(authenticate.verifyUser, (req, res, next)=>{
  Platos.create(req.body)
  .then((plato)=>{
    console.log('Plato creado', plato);
    res.statusCode = 200;
    res.setHeader('Content-Type','application/json');
    res.json(plato);
  }, (err) => next (err))
  .catch((err) => next(err));
})
.put(authenticate.verifyUser,(req, res, next)=>{
  res.statusCode = 403;
  res.end('PUT no es una operación soportada en /platos');
})
.delete(authenticate.verifyUser,(req, res, next)=>{
  Platos.remove({})
  .then((resp)=>{
    res.statusCode=200;
    res.setHeader('Content-Type','application/json');
    res.json(resp);
  }, (err) => next(err))
  .catch((err)=>next(err));
});

platoRouter.route('/:platoId')
.get((req, res, next)=>{
  Platos.findById(req.params.platoId)
  .then((plato)=>{
    res.statusCode = 200;
    res.setHeader('Content-Type','application/json');
    res.json(plato);
  }, (err)=> next(err))
  .catch((err)=>console.log(err));
})
.post(authenticate.verifyUser,(req, res, next)=>{
  res.statusCode = 403;
  res.end('POST no es soportado en /platos/'+req.params.platoId);
})
.put(authenticate.verifyUser,(req, res, next) => {
  Platos.findByIdAndUpdate(req.params.platoId,{
    $set: req.body
  },{new: true})
  .then((plato)=>{
    res.statusCode = 200;
    res.setHeader('Content-Type','application/json');
    res.json(plato);
  }, (err)=> next(err))
  .catch((err)=>{
    console.log('Momentito, hay error!! \n');
    console.log(err);
  });
})
.delete(authenticate.verifyUser,(req, res, next)=>{
  Platos.findByIdAndRemove(req.params.platoId)
  .then((resp)=>{
    res.statusCode= 200;
    res.setHeader('Content-Type','application/json');
    res.json(resp);
  }, (err)=> next(err))
  .catch((err)=>next(err));
});

platoRouter.route('/:platoId/comments')
.get((req,res,next)=>{
  Platos.findById(req.params.platoId)
  .then((plato)=>{
    if(plato != null){
      res.statusCode= 200;
      res.setHeader('Content-Type','application/json');
      res.json(plato.comments);
    }
    else{
      err= new Error ('Plato con el ID: ' + req.params.platoId + 'no existe');
      err.status=404;
      return next(err);
    }
  }, (err)=> next(err))
  .catch((err)=> next(err));
})
































module.exports = platoRouter;
